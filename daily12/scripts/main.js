// respond to button click
console.log("JS Initiated")

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Inside getFormInfo")
    var team1_text = document.getElementById('team1-text').value;
    var team2_text = document.getElementById('team2-text').value;
    var game_review = document.getElementById('game-review').value;
    console.log('team 1:' + team1_text + ' team 2: ' + team2_text + ' review: ' + game_review);

    // get radio state
    var winner_string = "";
    if (document.getElementById('radio-team1-value').checked){
        console.log('detected team1');
        winner_string = "Team 1,";
    }

    if (document.getElementById('radio-team2-value').checked) {
        console.log('detected team2');
        winner_string = "Team 2,";
    }

    if (document.getElementById('radio-draw-value').checked) {
        console.log('detected draw!');
        winner_string = "Draw";
    }
    // make genre combined string
    console.log('winner: ' + winner_string);

    // make dictionary
    game_dict = {};
    game_dict['team1'] = team1_text;
    game_dict['team2'] = team2_text;
    game_dict['review'] = game_review;
    game_dict['winner'] = winner_string;
    console.log(game_dict);

    displayReview(game_dict);

}

function displayReview(game_dict){
    console.log('Inside displayReview');
    console.log(game_dict);
    // get fields from review and display in label.
    var review_top = document.getElementById('review-top-line');
    review_top.innerHTML = game_dict['team1'] + ' versus ' + game_dict['team2'];

    var review_body = document.getElementById('review-body');
    review_body.innerHTML = game_dict['review'];

}
