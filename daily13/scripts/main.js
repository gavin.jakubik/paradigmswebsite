// respond to button click
console.log("JS Initiated");

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = makePubIpNetworkCall;

function makePubIpNetworkCall(){
    console.log("Making public ip address api request");
    
    // networking
    //set url for first request
    
    var xhr = new XMLHttpRequest();
    var url = 'https://api.ipify.org?format=json';
    
    xhr.open("GET", url, true); // true for async
    
    xhr.onload = function(e){
        console.log(xhr.responseText);
        // update location with response
        updateIpText(xhr.responseText);
    }
    
   /* xhr.onerror(e){
        console.error(xhr.statusText);
    } */
    
    xhr.send(null); //we pass body here
    
}

function updateIpText(ipAddr){
    console.log("Making ip location request with response" + ipAddr);
    
    // parse ipaddr as json
    var responseJSON = JSON.parse(ipAddr)
    
    // update dom
    var label1 = document.getElementById("review-top-line");
    
    if(responseJSON['ip'] == null){
        label1.innerHTML = "Unable to get ip address";
    }
    else{
        label1.innerHTML = responseJSON['ip'];
        makeIpLocNetworkCall(responseJSON['ip']);
    }
    
    //'https://ipinfo.io/' + responseJSON['ip']
    
    
}

function makeIpLocNetworkCall(ipStr){
    var xhr = new XMLHttpRequest();
    var url = 'https://ipinfo.io/' + ipStr + '/json';
    
    xhr.open("GET", url, true); // true for async
    
    xhr.onload = function(e){
        console.log(xhr.responseText);
        // update location with response
        updateLocationText(xhr.responseText);
    }
    
   /* xhr.onerror(e){
        console.error(xhr.statusText);
    } */
    
    xhr.send(null); //we pass body here
}

function updateLocationText(text_response){
    var responseJSON = JSON.parse(text_response)
    
    // update dom
    var label1 = document.getElementById("review-second-line");
    
    if(responseJSON['city'] == null || responseJSON['region'] == null){
        label1.innerHTML = "Unable to get location information";
    }
    else{
        label1.innerHTML = responseJSON['city'] + ', ' + responseJSON['region'];
    }
}

function getFormInfo(){
    console.log("Inside getFormInfo");
    var team1_text = document.getElementById('team1-text').value;
    var team2_text = document.getElementById('team2-text').value;
    var game_review = document.getElementById('game-review').value;
    console.log('team 1:' + team1_text + ' team 2: ' + team2_text + ' review: ' + game_review);

    // get radio state
    var winner_string = "";
    if (document.getElementById('radio-team1-value').checked){
        console.log('detected team1');
        winner_string = "Team 1,";
    }

    if (document.getElementById('radio-team2-value').checked) {
        console.log('detected team2');
        winner_string = "Team 2,";
    }

    if (document.getElementById('radio-draw-value').checked) {
        console.log('detected draw!');
        winner_string = "Draw";
    }
    // make genre combined string
    console.log('winner: ' + winner_string);

    // make dictionary
    game_dict = {};
    game_dict['team1'] = team1_text;
    game_dict['team2'] = team2_text;
    game_dict['review'] = game_review;
    game_dict['winner'] = winner_string;
    console.log(game_dict);

    displayReview(game_dict);

}


