import requests
import json
import unittest

class TestServer(unittest.TestCase):
    SITE_URL = 'http://localhost:51068'
    EVENT_URL = SITE_URL + '/events/'
    COUNTRIES_URL = SITE_URL + '/countries/'
    YEARS_URL = SITE_URL + '/years/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_events_index_get(self):

        r = requests.get(self.EVENT_URL)
        #self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        third_event = resp['data'][2]

        third_event_year = 1970
        third_event_country = 'Philippines'

        self.assertEqual(third_event['year'], third_event_year)
        self.assertEqual(third_event['country_txt'], third_event_country)

    def test_events_key_get(self):

        r = requests.get(self.EVENT_URL + '197000000001')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        first_event = resp['data']

        first_event_year = 1970
        first_event_country = 'Dominican Republic'

        self.assertEqual(first_event['year'], first_event_year)
        self.assertEqual(first_event['country_txt'], first_event_country)

    def test_countries_index_get(self):

        r = requests.get(self.COUNTRIES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        third_country_name = "Algeria"

        self.assertEqual(third_country_name, resp['data'][2])
    
    def test_years_index_get(self):

        r = requests.get(self.YEARS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        first_year = 1970

        self.assertEqual(first_year, resp['data'][0])

    def test_countries_key_get(self):

        r = requests.get(self.COUNTRIES_URL + 'Brazil')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        first_desc = "Hostage Taking (Kidnapping)"

        self.assertEqual(resp['data'][0]['attacktype1_txt'], first_desc)

    
    def test_years_key_get(self):

        r = requests.get(self.YEARS_URL + '1994')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        first_desc = "Facility/Infrastructure Attack"

        self.assertEqual(resp['data'][0]['attacktype1_txt'], first_desc)


    if __name__ == "__main__":
        unittest.main()
    #print(json.loads(requests.get(EVENT_URL + '201712310032').content.decode()))

    #print(len(json.loads(requests.get(COUNTRIES_URL + 'United States').content.decode())['data']))

    #print(json.loads(requests.get(YEARS_URL + '1970').content.decode()))




