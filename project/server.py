import cherrypy
from terrorism_library import _terrorism_database
from events_controller import EventController
from countries_controller import CountriesController
from years_controller import YearsController

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    tdb = _terrorism_database('terrorismData.csv')

    eventController = EventController(tdb)
    countriesController = CountriesController(tdb)
    yearsController = YearsController(tdb)

    dispatcher.connect('event_index_get', '/events/', controller=eventController, action='GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('event_get', '/events/:eventid', controller=eventController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('country_get', '/countries/:country', controller=countriesController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('year_get', '/years/:year', controller=yearsController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('countries_get', '/countries/', controller=countriesController, action='GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('years_get', '/years/', controller=yearsController, action='GET_INDEX', conditions=dict(method=['GET']))
    conf = {
	'global': {
        'server.thread_pool': 5, # optional argument
        'server.socket_host': 'gavinjakubik.me', #
	    'server.socket_port': 51068, #change port number to your assigned
	    },
	'/': {
	    'request.dispatch': dispatcher,
        'tools.CORS.on': True
	    }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# end of start_service

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()


