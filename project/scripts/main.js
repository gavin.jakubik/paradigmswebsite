// JavaScript code for final project
console.log('entered main.js');
var uri = "http://gavinjakubik.me:51068";
onStart(uri);

var filterButton = document.getElementById('filter-button');
filterButton.onclick = filterEvents;

function onStart(uri){

    // call functions that fill options dropdowns
    makeCountriesOptionsRequestToServer(uri);
    makeYearsOptionsRequestToServer(uri);
    filterEvents();

}

function filterEvents(){
    console.log('entered filterEvents');
    var uri = "http://gavinjakubik.me:51068";
    var year = document.getElementById('year-filter').value;
    var country = document.getElementById('country-filter').value;
    
    console.log('Year:' + year);
    console.log('Country:' + country);

    makeEventsRequestToServer(uri, year, country);
}

function makeEventsRequestToServer(uri, year, country){
    console.log('entered makeEventsRequestToServer');

    //decide wether to group by country or year
    if(country == 'Select' & year == 'Select'){
        // if unspecified, list all events
        requestEvents(uri + '/events/');
    }else if(country == 'Select'){
        requestEvents(uri + '/years/' + year);
    }else if(year == 'Select'){
        requestEvents(uri + '/countries/' + country);
    }else{
        requestByBoth(uri + '/years/' + year, country);
    }
}

function requestEvents(uri){
    console.log('Entered requestEvents');

    var xhr = new XMLHttpRequest();

    // TODO request using the year and/or country parameters
    // we want city and description
    
    console.log("Uri: " + uri);

    xhr.open("GET", uri, true);

    xhr.onload = function(e){
        console.log(xhr.responseText);
        // update response, listresponse returns list of dicts
        addToEventFrame(listResponse(xhr.responseText));
    }

    xhr.send(null);
    
}

function requestByBoth(uri, country){
    console.log('Entered requestByBoth');

    var xhr = new XMLHttpRequest();

    // TODO request using the year and/or country parameters
    // we want city and description
    
    console.log("Uri: " + uri);

    xhr.open("GET", uri, true);

    xhr.onload = function(e){
        console.log(xhr.responseText);
        // update response, listresponse returns list of dicts
        addToFrameByBoth(listResponse(xhr.responseText), country);
    }

    xhr.send(null);
    
}

function addToFrameByBoth(eventDictList, country){
    console.log("Adding Events to table");
    var oldEventTable = document.getElementById('eventsTable');
    var newEventTable = document.createElement('tbody');
    newEventTable.setAttribute('style', 'overflow-y: scroll; height: 40px');
    newEventTable.setAttribute('id', 'eventsTable');

    for (var i = 0; i < eventDictList.length; i++){
        if(eventDictList[i]['country_txt'] == country){
            console.log("adding new event");
        // add options to year dropdown menu
            var tableEntry = document.createElement('tr');
        
            var eventYear = document.createElement('td');
            var eventYearText = document.createTextNode(eventDictList[i]['year']);
            eventYear.appendChild(eventYearText);
            tableEntry.appendChild(eventYear);

            var eventCountry = document.createElement('td');
            var eventCountryText = document.createTextNode(eventDictList[i]['country_txt']);
            eventCountry.appendChild(eventCountryText);
            tableEntry.appendChild(eventCountry);

            var eventCity = document.createElement('td');
            var eventCityText = document.createTextNode(eventDictList[i]['city']);
            eventCity.appendChild(eventCityText);
            tableEntry.appendChild(eventCity);

            var eventDesc = document.createElement('td');
            var eventDescText = document.createTextNode(eventDictList[i]['attacktype1_txt']);
            eventDesc.appendChild(eventDescText);
            tableEntry.appendChild(eventDesc);

            newEventTable.appendChild(tableEntry);
        }
    }

    oldEventTable.parentNode.replaceChild(newEventTable, oldEventTable);
}


    
function addToEventFrame(eventDictList){
    console.log("Adding Events to table");
    var oldEventTable = document.getElementById('eventsTable');
    var newEventTable = document.createElement('tbody');
    newEventTable.setAttribute('style', 'overflow-y: scroll;');
    newEventTable.setAttribute('id', 'eventsTable');

    var jump = 1;
    if(eventDictList.length == 181691){
        jump = 1200;
    }
    for (var i = 0; i < eventDictList.length; i += jump){
        console.log("adding new event");
        // add options to year dropdown menu
        var tableEntry = document.createElement('tr');
        
        var eventYear = document.createElement('td');
        var eventYearText = document.createTextNode(eventDictList[i]['year']);
        eventYear.appendChild(eventYearText);
        tableEntry.appendChild(eventYear);

        var eventCountry = document.createElement('td');
        var eventCountryText = document.createTextNode(eventDictList[i]['country_txt']);
        eventCountry.appendChild(eventCountryText);
        tableEntry.appendChild(eventCountry);

        var eventCity = document.createElement('td');
        var eventCityText = document.createTextNode(eventDictList[i]['city']);
        eventCity.appendChild(eventCityText);
        tableEntry.appendChild(eventCity);

        var eventDesc = document.createElement('td');
        var eventDescText = document.createTextNode(eventDictList[i]['attacktype1_txt']);
        eventDesc.appendChild(eventDescText);
        tableEntry.appendChild(eventDesc);

        newEventTable.appendChild(tableEntry);
    }

    oldEventTable.parentNode.replaceChild(newEventTable, oldEventTable);
}

function makeYearsOptionsRequestToServer(uri){
    console.log('entered makeYearsOptionsRequestToServer');

    // create new request
    var xhr = new XMLHttpRequest();

    // TODO request using the year and/or country parameters
    // we want city and description
    
    uri += '/years/'
    console.log("Uri: " + uri);

    xhr.open("GET", uri, true);

    xhr.onload = function(e){
        console.log(xhr.responseText);
        // update response
        addToYearFilter(listResponse(xhr.responseText));
    }

    xhr.send(null);
    
}

function makeCountriesOptionsRequestToServer(uri){
    console.log('entered makeCountriesOptionsRequestToServer');

    // create new request
    var xhr = new XMLHttpRequest();

    // TODO request using the year and/or country parameters
    // we want city and description
    
    uri += '/countries/'

    console.log("Uri: " + uri);


    xhr.open("GET", uri, true);

    xhr.onload = function(e){
        console.log(xhr.responseText);
        // update response
        addToCountryFilter(listResponse(xhr.responseText));
    }

    xhr.send(null);
    
}

function addToYearFilter(listResponse){
    var yearDropdown = document.getElementById('year-filter');
    for (var i = 0; i < listResponse.length; i++){
        // add options to year dropdown menu
        var option = document.createElement('option');
        option.text = listResponse[i];
        yearDropdown.add(option);
    }
}

function addToCountryFilter(listResponse){
    var countryDropdown = document.getElementById('country-filter');
    for (var i = 0; i < listResponse.length; i++){
        // add options to country dropdown menu
        var option = document.createElement('option');
        option.text = listResponse[i];
        countryDropdown.add(option);
    }
}

function listResponse(responseStr){
    
    var responseObj = JSON.parse(responseStr);
    if(responseObj['result'] == 'success'){
        console.log("Returning data from list response, len: " + responseObj['data'].length);
        return responseObj['data'];
    }else{
        return ["Filter unable to load"];
    }
}

function displayResults(response_text){
    console.log('Results: ' + response_text);

    // TODO display the results
    // tricky JS since we need to make table resizable depending on # of results
}
