Repository for collaboration with group mates on class assignments in Programming Paradigms


**OO API**

Our program extracts various pieces of information from our data source. The way we store the information is strategic as to be efficient when we later allow the user to interact with the data. The user is able to filter through the data by a variety of factors, including year or the incident and country where the incident took place. When these filters are put in to place, our program will query the dictionary to identify the desired results. The surver makes request to the endpoint using one of three parameters: event id, country, and year. A json object is returned.

To run the tests, run the following command: python3 test_server.py

Note: The [pandas] module is necessary for running the server. 


**JSON Specification**

| Request Type | Resource Endpoint | Body | Expected Response | Inner Working of Handler |
| ------ | ------ | ------ | ------ | ------ |
| GET | /events/ | No Body | String formatted json array of all events (each is event is a dictionary containing event info) | GET_INDEX Returns json formatted string of full database |
| GET | /events/eventid | No Body | String formatted json of specified event | GET_KEY Returns db indexed to key given by eventid |
| GET | /countries/ | No Body | String formatted json whith list of all countries contained in the data | GET_INDEX uses pandas groupby on "countries_txt" field and then returns the groups produced |
| GET | /countries/country | No Body | String formatted json of all events in a given country | GET_KEY uses pandas groupby on "countries_txt" field and then returns the group specified by the user |
| GET | /years/ | No Body | String formatted json whith list of all years contained in the data | GET_INDEX uses pandas groupby on "year" field and then returns the groups produced |
| GET | /years/year | No Body | String formatted json of all events in a given year | GET_KEY uses pandas groupby on "year" field and then returns the group specified by the user |


**User Interaction**

The user is able to use our interface to filter through our data source containing a wide variety of terrorism attacks across the globe from 1970-2017. Initially, the webpage opens to our home page. From there, the user can chose to redirect to the page where the filter options are. On that page, the user can chose a year and/or a country to display results for. Once the "Filter" button is clicked, a table is populated with the years, countries, cities, and descriptions of the invidents that match the user search from our server. Additionally, we have about pages linked to several different pages at the top of our navigation bar. This provides the user with easy access to additional information.


**Complexity**

Our project uses a massive data source which we parse through and extract necessary information from. Using javascript, our front-end connects to the back-end where we tap into the server we built. This server requires pandas to run, adding a level of complexity to our project. The navigation bar provides the user with different outlets to receive more information, making our interface appear to be a real-world website at production quality. When filtering through the data, the user can access legitimate information that can be applied to a variety of concepts today. 


**Presentation Slides**

Can be found [here](https://docs.google.com/presentation/d/18TpD74RRnbEUhS5qWktLFyF0LnsIKnDh3m3m90ThLQY/edit#slide=id.p).


[pandas]: https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html